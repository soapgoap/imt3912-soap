// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GoapTargetModule.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "GoapPlanner.h"
#include "GoapUtilityEvaluator.h"
#include "GoapActionBase.h"
#include "GoapAIController.generated.h"


/**
 * Base class for all Goap AiControllers. 
 */
UCLASS()
class GOAPTARGET_API AGoapAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	AGoapAIController();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	

	UFUNCTION(BlueprintCallable, Category = "Action|Function")
	void ActorReady();			//Actor is ready to receive work
	UFUNCTION(BlueprintCallable, Category = "Action|Function")
	void ActorInterrupt();		//Plan is invalid

protected:
	TArray<UGoapActionBase*> ActionSet;
	TArray<UGoapActionBase*> CompleteActionPlan;

	UPROPERTY(EditAnywhere, Category="GOAP")
	UBlackboardData* BlackboardDataAsset;

	UPROPERTY()
	UGoapPlanner* GoapPlanner;

	UPROPERTY()
	UGoapUtilityEvaluator* GoalSelector;

	void SetGoal(TMap<FName, bool> GoalState);
	
	UPROPERTY(EditAnywhere, Category = "GOAP")
	TMap<FName, bool> MapInBP;

private:
	bool bReady;
	uint8 CurrentActionIndex = 0;
	TMap<FName, bool> PlanEndGoal;
	float TimeAccumulator;
};
