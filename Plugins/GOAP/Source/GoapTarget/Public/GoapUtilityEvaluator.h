// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GoapTargetModule.h"

#include "EngineMinimal.h"
#include "Object.h"
#include "UObject/NoExportTypes.h"
#include "GoapUtilityGraphBase.h"
#include "GoapUtilityGraphLinear.h"
#include "GoapUtilityGraphExponential.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "GoapUtilityEvaluator.generated.h"

/**
 * 
 */
UCLASS()
class GOAPTARGET_API UGoapUtilityEvaluator : public UObject
{
	GENERATED_BODY()

public:
	bool SetBlackboard(UBlackboardComponent*& ParentBlackboard);
	TPair<bool, TMap<FName, bool>> EvaluateAllGraphs();
	

	void AddLinearEvaluation(TMap<FName, bool> Goal, FName Input, float Multiplier, float Constant = 0.0f);
	void AddExponentialEvaluation(TMap<FName, bool> Goal, FName Input, float Exponent, float Constant = 0.0f);

	bool HaveGraphsToEvaluate();
protected:
	UPROPERTY()
	UBlackboardComponent* Blackboard;
	TArray<UGoapUtilityGraphBase*> Graphs;
private:
	int16 PreviousGoalIndex = -1;
	TMap<FName, bool> GetGoalAtIndex(int16 Index);
};
