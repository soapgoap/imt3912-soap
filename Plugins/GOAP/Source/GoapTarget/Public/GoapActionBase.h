// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GoapTargetModule.h"

#include "UObject/NoExportTypes.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "GoapActionBase.generated.h"

/** Base class for all goap actions
 * 
 */
UCLASS(abstract, Blueprintable, meta = (BlueprintSpawnableComponent), ClassGroup = GOAP)
class GOAPTARGET_API UGoapActionBase : public UActorComponent
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Execute();

	UPROPERTY(EditAnywhere, Category = "GOAP")
	TMap<FName, bool> PreConditions;
	UPROPERTY(EditAnywhere, Category = "GOAP")
	TMap<FName, bool> PostConditions;

	FName Name;

	UFUNCTION(BlueprintCallable)
	bool SetBlackboard(UBlackboardComponent*& ParentBlackboard);
protected:
	UBlackboardComponent* Blackboard;
};
