// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapTargetModule.h"

#include "GoapActionBase.h"

void UGoapActionBase::Execute_Implementation()
{
	unimplemented()
}

bool UGoapActionBase::SetBlackboard(UBlackboardComponent *& ParentBlackboard)
{
	check(ParentBlackboard != nullptr);
	Blackboard = ParentBlackboard;

	return true;
}
