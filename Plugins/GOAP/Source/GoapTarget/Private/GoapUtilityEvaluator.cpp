// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapTargetModule.h"

#include "GoapUtilityEvaluator.h"




bool UGoapUtilityEvaluator::SetBlackboard(UBlackboardComponent* &ParentBlackboard)
{
	check(ParentBlackboard != nullptr);

	Blackboard = ParentBlackboard;
	return true;
}

/** The bool in the return value indicate whether or not there was a change from the previous evaluation.
 *  
*/

TPair<bool, TMap<FName, bool>> UGoapUtilityEvaluator::EvaluateAllGraphs()
{
	float HighestScore = -5.f;
	int16 Index = -1;
	for (int i = 0; i < Graphs.Num(); i++)
	{
		float NewScore = 0;
		NewScore = Graphs[i]->Evaluate();
		if (NewScore > HighestScore)
		{
			Index = i;
			HighestScore = NewScore;
		}
	}

	TPair<bool, TMap<FName, bool>> NewGoal;
	NewGoal.Key = true;
	if (Index == PreviousGoalIndex)
	{
		NewGoal.Key = false;
	}

	PreviousGoalIndex = Index;
	NewGoal.Value = GetGoalAtIndex(Index);
	return NewGoal;
}

void UGoapUtilityEvaluator::AddLinearEvaluation(TMap<FName, bool> Goal, FName Input, float Multiplier, float Constant)
{
	UPROPERTY()
	UGoapUtilityGraphLinear* NewGraph = NewObject<UGoapUtilityGraphLinear>(this);

	NewGraph->Goal = Goal;
	NewGraph->InputKey = Input;
	NewGraph->Multiplier = Multiplier;
	NewGraph->Constant = Constant;
	NewGraph->SetBlackboard(Blackboard);

	Graphs.Add(NewGraph);
}

void UGoapUtilityEvaluator::AddExponentialEvaluation(TMap<FName, bool> Goal, FName Input, float Exponent, float Constant)
{
	UPROPERTY()
	UGoapUtilityGraphLinear* NewGraph = NewObject<UGoapUtilityGraphLinear>(this);

	NewGraph->Goal = Goal;
	NewGraph->InputKey = Input;
	NewGraph->Multiplier = Exponent;
	NewGraph->Constant = Constant;
	NewGraph->SetBlackboard(Blackboard);

	Graphs.Add(NewGraph);
}

bool UGoapUtilityEvaluator::HaveGraphsToEvaluate()
{
	return Graphs.Num() != 0;
}

TMap<FName, bool> UGoapUtilityEvaluator::GetGoalAtIndex(int16 Index)
{

	return Graphs[Index]->Goal;
}