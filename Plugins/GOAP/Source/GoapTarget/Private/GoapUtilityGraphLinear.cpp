// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapTargetModule.h"

#include "GoapUtilityGraphLinear.h"




float UGoapUtilityGraphLinear::Evaluate_Implementation()
{
	float Score = 0.0f;

	float Input = Blackboard->GetValueAsFloat(InputKey);

	Score = (Multiplier * Input) + Constant;

	return Score;
}