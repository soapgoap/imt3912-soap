// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapTargetModule.h"

#include "GoapAIController.h"



AGoapAIController::AGoapAIController()
{
	Blackboard = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComponent"));

	FName ActorIsHungryKey = "ActorIsHungry";
	FName ActorHasKnowledge = "ActorHasKnowledge";



	// We want the agent to stop being hungry 
	TMap<FName, bool> FindFood;
	FindFood.Add(ActorIsHungryKey, false);
	TMap<FName, bool> Explore;
	Explore.Add(ActorHasKnowledge, false);

	SetGoal(FindFood);

	bReady = false;	//Wait for actor to report they are ready to receive actions
	TimeAccumulator = 0;
}

void AGoapAIController::BeginPlay()
{
	Super::BeginPlay();

	if (BlackboardDataAsset == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("No blackboard set for %s"), *GetName());
	}
	else
	{
		Blackboard->InitializeBlackboard(*BlackboardDataAsset);

		FName HungerValueKey = "HungerValue";
		FName FoodOnGroundKey = "FoodOnGround";
		FName ActorHasFoodKey = "ActorHasFood";
		FName ActorIsHungryKey = "ActorIsHungry";
		FName MachineIsPoweredKey = "MachineIsPowered";
		FName ActorHasMoneyKey = "ActorHasMoney";

		GoapPlanner = NewObject<UGoapPlanner>(this);
		GoapPlanner->SetBlackboard(Blackboard);

		GetPawn()->GetComponents<UGoapActionBase>(ActionSet);
		GoapPlanner->SetAvailableActions(ActionSet);

		GoalSelector = NewObject<UGoapUtilityEvaluator>(this);
		GoalSelector->SetBlackboard(Blackboard);


		TMap<FName, bool> FindFood;
		FindFood.Add(ActorIsHungryKey, false);

		GoalSelector->AddLinearEvaluation(FindFood, HungerValueKey, 1.f);


		TMap<FName, bool> DontFindFood;
		DontFindFood.Add(ActorIsHungryKey, true);

		//GoalSelector->AddLinearEvaluation(DontFindFood, HungerValueKey, 0.5f, 2.f);


		Blackboard->SetValueAsBool(FoodOnGroundKey, false);
		Blackboard->SetValueAsBool(ActorHasFoodKey, false);
		Blackboard->SetValueAsBool(ActorIsHungryKey, true);
		Blackboard->SetValueAsBool(MachineIsPoweredKey, false);
		Blackboard->SetValueAsBool(ActorHasMoneyKey, false);

		bReady = true;
	}
}

void AGoapAIController::Tick(float DeltaTime)
{
	TimeAccumulator += DeltaTime;
	if (TimeAccumulator > 2.f)
	{
		TimeAccumulator = 0.f;
		FName HungerValueKey = "HungerValue";
		float Hunger = Blackboard->GetValueAsFloat(HungerValueKey);
		Hunger += 1.f;
		Blackboard->SetValueAsFloat(HungerValueKey, Hunger);
	}
	if (bReady) 
	{
		if (GoalSelector->HaveGraphsToEvaluate())
		{
			TPair<bool, TMap<FName, bool>> NewGoal;
			NewGoal = GoalSelector->EvaluateAllGraphs();
			if (NewGoal.Key == true)
			{
				ActorInterrupt();
				SetGoal(NewGoal.Value);
				ActorReady();
				UE_LOG(LogTemp, Warning, TEXT("Goals have changed, goal is:"));
				for (auto& Goal : NewGoal.Value)
				{
					UE_LOG(LogTemp, Warning, TEXT("\t%s"), *Goal.Key.ToString());
				}
			}
		}
		if (CompleteActionPlan.Num() > CurrentActionIndex)
		{
			bReady = false;
			//CompleteActionPlan[CurrentActionIndex].Function.ExecuteIfBound();
			UE_LOG(LogTemp, Warning, TEXT("Trying to execute action %s"), *CompleteActionPlan[CurrentActionIndex]->GetName());
			CompleteActionPlan[CurrentActionIndex]->Execute();
			CurrentActionIndex++;
		}
		else
		{
			CurrentActionIndex = 0;
			GoapPlanner->SetAvailableActions(ActionSet);
			CompleteActionPlan = GoapPlanner->MakeNewPlan(PlanEndGoal);
			//UE_LOG(LogTemp, Warning, TEXT("Current plan is:"));
			//for (auto& Step : CompleteActionPlan)
			//{
			//	UE_LOG(LogTemp, Warning, TEXT("\t%s"), *Step->Name.ToString());
			//}
		}
	}
}



void AGoapAIController::ActorReady()
{
	bReady = true;
}

void AGoapAIController::ActorInterrupt()
{
	bReady = false;
	//CurrentActionIndex = 0;
	CompleteActionPlan.Empty();
}

void AGoapAIController::SetGoal(TMap<FName, bool> GoalState)
{
	PlanEndGoal = GoalState;
}