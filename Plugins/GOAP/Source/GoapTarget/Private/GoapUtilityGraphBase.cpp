// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapTargetModule.h"

#include "GoapUtilityGraphBase.h"




bool UGoapUtilityGraphBase::SetBlackboard(UBlackboardComponent* &ParentBlackboard)
{
	check(ParentBlackboard != nullptr);

	Blackboard = ParentBlackboard;
	return true;

}

float UGoapUtilityGraphBase::Evaluate_Implementation()
{
	unimplemented();
	return 0.0f;
}
