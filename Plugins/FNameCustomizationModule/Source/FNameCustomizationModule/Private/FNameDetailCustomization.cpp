#include "FNameCustomizationModule.h"
#include "FNameDetailCustomization.h"

//Copypaste from RangeStructCustomization
#include "Widgets/DeclarativeSyntaxSupport.h"
#include "Engine/GameViewportClient.h"
#include "Widgets/SBoxPanel.h"
#include "Widgets/Layout/SSpacer.h"
#include "Widgets/Text/STextBlock.h"
#include "Widgets/Layout/SBox.h"
#include "Widgets/Input/SComboBox.h"
#include "DetailWidgetRow.h"
#include "Editor.h"
#include "PropertyHandle.h"
#include "DetailLayoutBuilder.h"
#include "Widgets/Input/SNumericEntryBox.h"
#include "SlateBasics.h"

void FNameDetailCustomization::CustomizeHeader(TSharedRef<IPropertyHandle> StructPropertyHandle, FDetailWidgetRow & HeaderRow, IPropertyTypeCustomizationUtils & StructCustomizationUtils)
{
	PropertyHandle = StructPropertyHandle;
	
	FText Data;
	PropertyHandle->GetValueAsFormattedText(Data);
	if (FName(*Data.ToString(), FNAME_Find) != NAME_None)
	{
		ThisName = Exists;
	}

	HeaderRow.NameContent()
	[
		StructPropertyHandle->CreatePropertyNameWidget()
	]
	.ValueContent()
	.MinDesiredWidth(200.0f)
	.MaxDesiredWidth(400.0f)
	[
		SNew(SHorizontalBox)
		+ SHorizontalBox::Slot()
		[
			SNew(SEditableTextBox)
			.Text(this, &FNameDetailCustomization::GetNameValue)
			.OnTextChanged(this,&FNameDetailCustomization::UpdateValue)
			.OnTextCommitted(this,&FNameDetailCustomization::SetNameValue)
		]
		+ SHorizontalBox::Slot()
			.Padding(FMargin(2.0f,3.0f))
		[
			SNew(STextBlock)
			.Text(this, &FNameDetailCustomization::IsUnique)
		]
	]
	;
}

void FNameDetailCustomization::CustomizeChildren(TSharedRef<class IPropertyHandle> StructPropertyHandle, IDetailChildrenBuilder & StructBuilder, IPropertyTypeCustomizationUtils & StructCustomizationUtils)
{
	//do nothing
}

FText FNameDetailCustomization::GetNameValue() const
{
	FText Data;
	PropertyHandle->GetValueAsFormattedText(Data);

	return Data;
}

void FNameDetailCustomization::UpdateValue(const FText & NewText)
{
	if (FName(*NewText.ToString(), FNAME_Find) == NAME_None)
	{
		ThisName = NoMatch;
	}
	else
	{
		ThisName = Exists;
	}
}

void FNameDetailCustomization::SetNameValue(const FText& NewText, ETextCommit::Type CommitInfo)
{
	if (FName(*NewText.ToString(), FNAME_Find) == NAME_None)
	{
		ThisName = IsNew;
	}
	
	PropertyHandle->SetValueFromFormattedString(*NewText.ToString());
}

FText FNameDetailCustomization::IsUnique() const
{
	switch(ThisName)
	{
	case IsNew:		return FText::FromString(TEXT("++"));
	case Exists:	return FText::FromString(TEXT("=="));	
	case NoMatch:	return FText::FromString(TEXT("!="));
	default:		return FText::FromString(TEXT(".."));
	}
}
