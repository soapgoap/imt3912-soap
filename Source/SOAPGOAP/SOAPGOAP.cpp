// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "SOAPGOAP.h"


IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SOAPGOAP, "SOAPGOAP" );

DEFINE_LOG_CATEGORY(LogSOAPGOAP)
 