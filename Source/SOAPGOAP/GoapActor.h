// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "GoapActor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGoapActionSetDelegate);

UCLASS()
class SOAPGOAP_API AGoapActor : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AGoapActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//GOAP - Variables
	UPROPERTY(BlueprintReadWrite)	bool bBusy;

	UFUNCTION(BlueprintCallable, Category = "Action|Function")								void ActionEat(int Healthgain);

	//GOAP - Events
	UFUNCTION(BlueprintImplementableEvent, Category = "Action|Event")						void MoveTo(FVector Destination);
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Action|Event")	void ActionPickup();
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Action|Event")	void ActionFindfood();

};
