// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "SOAPGOAPGameMode.generated.h"

UCLASS(minimalapi)
class ASOAPGOAPGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASOAPGOAPGameMode();
};



