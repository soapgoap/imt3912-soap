// Fill out your copyright notice in the Description page of Project Settings.

#include "SOAPGOAP.h"
#include "GoapActor.h"


// Sets default values
AGoapActor::AGoapActor()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGoapActor::BeginPlay()
{
	Super::BeginPlay();

	bBusy = false;
}

// Called every frame
void AGoapActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AGoapActor::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AGoapActor::ActionEat(int Healthgain)
{
	UE_LOG(LogTemp, Warning, TEXT("Gained %i health"), Healthgain)
}
