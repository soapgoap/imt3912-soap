// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "SOAPGOAP.h"
#include "Commander.h"
#include "AI/Navigation/NavigationSystem.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "SOAPGOAPCharacter.h"

ACommander::ACommander()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
}

void ACommander::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}
}

void ACommander::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("SelectUnique", IE_Pressed, this, &ACommander::SelectUnique);
	InputComponent->BindAction("Select", IE_Pressed, this, &ACommander::Select);

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &ACommander::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &ACommander::OnSetDestinationReleased);

	// support touch devices 
	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ACommander::MoveToTouchLocation);
	InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ACommander::MoveToTouchLocation);

	InputComponent->BindAction("ResetVR", IE_Pressed, this, &ACommander::OnResetVR);
}

void ACommander::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ACommander::MoveToMouseCursor()
{
	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	{
		if (ASOAPGOAPCharacter* MyPawn = Cast<ASOAPGOAPCharacter>(GetPawn()))
		{
			if (MyPawn->GetCursorToWorld())
			{
				UNavigationSystem::SimpleMoveToLocation(this, MyPawn->GetCursorToWorld()->GetComponentLocation());
			}
		}
	}
	else
	{
		// Trace to see what is under the mouse cursor
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);

		if (Hit.bBlockingHit)
		{
			// We hit something, move there
			SetNewMoveDestination(Hit.ImpactPoint);
		}
	}
}

void ACommander::MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	FVector2D ScreenSpaceLocation(Location);

	// Trace to see what is under the touch location
	FHitResult HitResult;
	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, HitResult);
	if (HitResult.bBlockingHit)
	{
		// We hit something, move there
		SetNewMoveDestination(HitResult.ImpactPoint);
	}
}

void ACommander::SetNewMoveDestination(const FVector DestLocation)
{
	APawn* const MyPawn = GetPawn();
	if (MyPawn)
	{
		UNavigationSystem* const NavSys = GetWorld()->GetNavigationSystem();
		float const Distance = FVector::Dist(DestLocation, MyPawn->GetActorLocation());

		// We need to issue move command only if far enough in order for walk animation to play correctly
		if (NavSys && (Distance > 120.0f))
		{
			//NavSys->SimpleMoveToLocation(this, DestLocation);


			//TODO: ISSUE TRAVEL DESTINATION TO AI CONTROLLER
			for (auto& Current : SelectedUnits)
			{
				Current->MoveTo(DestLocation);
			}
		}
	}
}

void ACommander::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void ACommander::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}

void ACommander::SelectUnique()
{
	SelectedUnits.Empty();
	Select();
}

void ACommander::Select()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_WorldDynamic, false, Hit);
	if (Hit.GetActor() != NULL) {
		AUnit* Selected = Cast<AUnit>(Hit.GetActor());
		if (Selected != NULL)
		{
			if (SelectedUnits.Find(Selected) != INDEX_NONE)
			{
				SelectedUnits.Remove(Selected);
			}
			else
			{
				SelectedUnits.AddUnique(Selected);
			}
			Selected = NULL;
		}
	}
}

